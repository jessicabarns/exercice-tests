import java.sql.SQLException;
import java.util.List;

public class EntrepriseFake implements EntrepriseInterface {
	public ConnexionDBFake db;

	public EntrepriseFake()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		connexion("server.com");
	}

	@Override
	public void connexion(String dburl)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		db = new ConnexionDBFake();
	}

	@Override
	public void deconnexion() throws SQLException {
		db = null;
	}

	@Override
	public void ajouterEmploye(int code, String nom, String prenom) throws SQLException {
		Employe employe = new Employe(code, nom, prenom);
		db.insert(employe);
	}

	@Override
	public void supprimerEmploye(int code) throws SQLException {
		db.delete(code);
	}

	@Override
	public String toString() {
		List<Employe> listEmployes = null;
		try {
			listEmployes = db.selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listEmployes.toString();
	}

	@Override
	public String toString(int code) throws Exception {
		Employe employe = db.select(code);
		return employe == null ? "" : employe.toString();
	}

}
