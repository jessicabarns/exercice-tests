import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class ConnexionDB implements ConnexionDBInterface {

    private Connection dbcon;
    private PreparedStatement pst;

    public ConnexionDB(String dburl) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        dbcon = DriverManager.getConnection("jdbc:" + dburl, "username", "password");
        pst = null;
    }
    
    public void close() throws SQLException {
        dbcon.close();
        dbcon = null;
    }

    public void insert(Employe e) throws SQLException {
        try {
            pst = dbcon.prepareStatement("INSERT INTO employees VALUES (?, ?, ?)");
            pst.setInt(1, e.ID);
            pst.setString(2, e.nom);
            pst.setString(3, e.prenom);
            pst.executeUpdate();
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
            resetStatement();
        }
    }

    public void delete(int code) throws SQLException {
        try {
            pst = dbcon.prepareStatement("DELETE FROM employees WHERE matricule=?");
            pst.setInt(1, code);
            pst.executeUpdate();
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
            resetStatement();
        }
    }

    public Employe select(int code) throws Exception {
        try {
            pst = dbcon.prepareStatement("SELECT * FROM employees WHERE matricule=?");
            pst.setInt(1, code);
            ResultSet rs = pst.executeQuery();
            if (!rs.first()) {
                throw new Exception("Employee " + code + " n'est pas inscrit dans la BD");
            }
            return new Employe(rs.getInt("matricule"), 
                               rs.getString("nom"), 
                               rs.getString("prenom"));
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
            resetStatement();
        }
        return null;
    }

    public List<Employe> selectAll() throws SQLException {
        try {
            pst = dbcon.prepareStatement("SELECT * FROM employees");
            ResultSet rs = pst.executeQuery();
            List<Employe> employees = new ArrayList<Employe>();
            while (rs.next()) {
                int code = rs.getInt("matricule");
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                employees.add(new Employe(code, nom, prenom));
            }
            return employees;
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
            resetStatement();
        }
        return null;
    }
    
    private void resetStatement() throws SQLException {
        if (pst != null) {
            pst.close();
            pst = null;
        }
    }
}
