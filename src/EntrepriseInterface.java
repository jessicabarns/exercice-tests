import java.sql.SQLException;
import java.util.List;

public interface EntrepriseInterface {
	       
    public void connexion(String dburl) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    public void deconnexion() throws SQLException;

    public void ajouterEmploye(int code, String nom, String prenom) throws SQLException;

    public void supprimerEmploye(int code) throws SQLException;

    public String toString(int code) throws Exception;

    public String toString();
}
