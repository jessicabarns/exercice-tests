import java.sql.SQLException;
import java.util.List;

public class Entreprise implements EntrepriseInterface {
    
    private ConnexionDB db;
    
    public Entreprise() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
    	connexion("server.com");
    }
    
    public void connexion(String dburl) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
    	db = new ConnexionDB(dburl);
    }

    public void deconnexion() throws SQLException {
    	db.close();
    }

    public void ajouterEmploye(int code, String nom, String prenom) throws SQLException {
    	Employe employe = new Employe(code, nom, prenom);
    	db.insert(employe);
    }

    public void supprimerEmploye(int code) throws SQLException {
    	db.delete(code);
    }

    public String toString(int code) throws Exception {
    	Employe employe = db.select(code);
    	return toString(employe);
    }
    
    private String toString(Employe e) {
    	return e.toString();
    }

    public String toString() {
    	List<Employe> listEmployes = null;
		try {
			listEmployes = db.selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
    	return listEmployes.toString();
    }
}
