import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnexionDBFake implements ConnexionDBInterface{
	public List<Employe> listEmployes;
	
	public ConnexionDBFake() {
		listEmployes = new ArrayList<Employe>();
	}

	@Override
	public void insert(Employe e) throws SQLException {
		listEmployes.add(e);		
	}

	@Override
	public void delete(int code) throws SQLException {
		for(Employe employe : listEmployes){
			if(employe.ID == code){
				listEmployes.remove(employe);
				break;
			}
		}
	}

	@Override
	public Employe select(int code) throws Exception {
		Employe employe = null;
		for(Employe e : listEmployes){
			if(e.ID == code){
				employe = e;
				break;
			}
		}
		return employe;
	}

	@Override
	public List<Employe> selectAll() throws SQLException {
		return listEmployes;
	}

}
