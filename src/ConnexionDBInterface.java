
import java.sql.SQLException;
import java.util.List;

public interface ConnexionDBInterface {
    
    

    public void insert(Employe e) throws SQLException;

    public void delete(int code) throws SQLException;

    public Employe select(int code) throws Exception;

    public List<Employe> selectAll() throws SQLException;
}