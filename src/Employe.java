public class Employe {

    public int ID;
    public String nom;
    public String prenom;
    
    public Employe(int ID, String nom, String prenom) {
        this.ID = ID;
        this.nom = nom;
        this.prenom = prenom;
    }

    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Employe)) {
            return false;
        }
        Employe other = (Employe)obj;
        return (other.ID == ID && other.nom == nom && other.prenom == prenom);
    }

	@Override
	public String toString() {
		return "Employe [ID=" + ID + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
    
}
