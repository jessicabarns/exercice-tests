import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.sql.SQLException;

public class EntrepriseTest {

    private EntrepriseFake OUT;
    
    @Before
    public void setup() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        OUT = new EntrepriseFake();
        // La création de la connexion à la DB se fait automatiquement en arrière plan...
    }
    
    @Test
    public void deconnexionTest() throws SQLException{
    	OUT.deconnexion();
    	assertTrue(OUT.db == null);
    }
    
    @Test
    public void ajouterEmployeTest() throws Exception{
    	addEmploye();
    	assertEquals(OUT.db.select(1).nom, "Jessica");
    }
    
    @Test
    public void supprimerEmployeTest() throws Exception{
    	addEmploye();
    	OUT.supprimerEmploye(1);
    	assertEquals(OUT.db.select(1), null);
    }
    
    @Test
    public void toStringTest() throws SQLException{
    	addEmploye();
    	assertEquals(OUT.toString(), "[Employe [ID=1, nom=Jessica, prenom=Barns]]");
    }
    
    @Test
    public void ToStringById() throws Exception{
    	addEmploye();
    	assertEquals(OUT.toString(1), "Employe [ID=1, nom=Jessica, prenom=Barns]");
    }
    
    @Test
    public void ToStringByIdWrongId() throws Exception{
    	addEmploye();
    	assertEquals(OUT.toString(2), "");
    }
    
    public void addEmploye() throws SQLException{
    	OUT.ajouterEmploye(1, "Jessica", "Barns");
    }
}
